/** @format */

import { Component } from 'react';
import { createPortal } from 'react-dom';

const modalRoot = document.getElementById('modal');

class Modal extends Component {
	constructor(props) {
		super(props);
		// This is the div that we're going to render into and stick inside the modal
		this.el = document.createElement('div');
	}

	componentDidMount() {
		modalRoot.appendChild(this.el);
	}

	componentWillUnmount() {
		// Need to cleanup after ourselves to avoid memory leaks and crashing the browser
		modalRoot.removeChild(this.el);
	}

	render() {
		return createPortal(this.props.children, this.el);
	}
}

export default Modal;
