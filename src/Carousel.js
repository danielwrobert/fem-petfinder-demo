/** @format */

import React, { Component } from 'react';

class Carousel extends Component {
	state = {
		photos: [],
		active: 0,
	};
	static getDerivedStateFromProps({ media }) {
		let photos = [];
		if (media && media.photos && media.photos.photo) {
			// "pn" is just the size of the photo from the API
			photos = media.photos.photo.filter(photo => photo['@size'] === 'pn');
		}

		return { photos };
	}
	handleIndexClick = event => {
		this.setState({
			// need to coerce this into being a number, otherwise the strict comparison on
			// className in the component (below) will always fail.
			active: +event.target.dataset.index,
		});
	};
	render() {
		const { photos, active } = this.state;
		return (
			<div className="carousel">
				<img src={photos[active].value} alt="animal" />
				<div className="carousel-smaller">
					{photos.map((photo, index) => (
						/* eslint-disable-next-line */
						<img
							onClick={this.handleIndexClick}
							data-index={index}
							key={photo.value}
							src={photo.value}
							className={index === active ? 'active' : ''}
							alt="animal thumbnail"
						/>
					))}
				</div>
			</div>
		);
	}
}

export default Carousel;
