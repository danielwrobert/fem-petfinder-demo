/** @format */

import React, { Component } from 'react';
import { ANIMALS } from 'petfinder-client'; // important that pf comes first
import { Consumer } from './SearchContext';

class SearchBox extends Component {
	handleFormSubmit = event => {
		event.preventDefault();
		this.props.search();
	};

	render() {
		return (
			<Consumer>
				{context => (
					<div className="search-params">
						<form onSubmit={this.handleFormSubmit}>
							<label htmlFor="location">
								Location
								<input
									onChange={context.handleLocationChange}
									id="location"
									value={context.location}
									placeholder="Location"
								/>
							</label>
							<label htmlFor="animal">
								Animal
								{/* if you have onChange, you also need to have onBlur */}
								<select
									id="animal"
									value={context.animal}
									onChange={context.handleAnimalChange}
									onBlur={context.handleAnimalChange}
								>
									{/* An empty option will reflect no choice made - as in "show all" */}
									<option />
									{/* Anytime you're doing a `map`, you need to provide a unique key, as mentioned before */}
									{ANIMALS.map(animal => (
										<option key={animal} value={animal}>
											{animal}
										</option>
									))}
								</select>
							</label>
							<label htmlFor="breed">
								Breed
								<select
									disabled={!context.breeds.length}
									id="breed"
									value={context.breed}
									onChange={context.handleBreedChange}
									onBlur={context.handleBreedChange}
								>
									<option />
									{context.breeds.map(breed => (
										<option key={breed} value={breed}>
											{breed}
										</option>
									))}
								</select>
							</label>
							<button>Submit</button>
						</form>
						<p>
							<em>
								You have chosen to search for{' '}
								{context.animal ? context.animal : 'pet'}s in {context.location}.
								Continue at your own risk!
							</em>
						</p>
					</div>
				)}
			</Consumer>
		);
	}
}

export default SearchBox;
